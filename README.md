## Назначение БД
База данных пациентов клиники. В ней можно хранить данные о визитах пациента, записи врача, исследования инструментальные и лабораторные


## Таблицы: 
| Таблица       |    Назначение | Дополнения |
|-------------| -------------|-------------|
| patients      |   Данные о пациенте |           |
| visits        |   Данные о визитах пациентов |           |
| research_list |   Список возможных исследований |           |
| research_type | Типы исследований |           |
| research      | Исследование пациента.   |           |
| research_param|  Список возможных параметров исследования  |           |
| research_param_select_type_list | Варианты ответов на параметры исследования типа список |           |
| research_param_value |  Значения параметров  |  Введено партиционирование. Таблица делится на пять частей. В зависимости от id  |
| users        |   Пользователи |   Врачи и прочий персонал, имеющий отношение к заполнению и обслуживанию БД        |


### Схема базы данных 

![Схема](scheme.png)


## Таблица visits

| Имя столбца   |    Тип         | Пояснение |
| ------------- | ------------- | ------------- |
| id            | bigint(20) unsigned NOT NULL AUTO_INCREMENT|  id визита  |  
| patient_id    | bigint(20) unsigned NOT NULL|  id пациента  |
| description   |  JSON NOT NULL  | Параметры визита   |
| date          |  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP |    |
| created_at    |  timestamp NULL DEFAULT NULL|    |
| updated_at    |  timestamp NULL DEFAULT NULL|    |
  
Для каждого визита пациента должны быть заполнены следующие параметры визита: 
* Давление
* Рост
* Вес
* Индекс массы
* Температура
По этой причине тип поля description заменен на JSON 


### Пример SELECT
Поиск пациентов c повышенной температурой

```sql
select * from visits where description->"$.temperature">37 and CAST(date as date)=curDate();
```
#### пример с left join
Для пациента с _id=2_ выберем все иследования и значения параметров  

```sql
select * from research LEFT JOIN research_param_value on research.id = research_param_value.research_id  where research.patient_id=2;
```

#### пример с inner join
выберем параметры и значения параметров для исследования research_id=2

```sql
select * from research_param LEFT JOIN research_param_value on research_param.id= research_param_value.research_param_id where research_param_value.research_id=2;
```

#### пять различных where

День рождения пациента в ближайшие 30 дней (оператор BETWEEN)
```sql
select name, secondname from patients WHERE (DAYOFYEAR(birthdate)-DAYOFYEAR(CURRENT_DATE)+365) MOD 365 BETWEEN 0 AND 30
```

Поиск пациентов возраст которых превысил 30 лет (оператор <)
```sql
SELECT
   name, secondname
  
FROM patients

where (
          (YEAR(CURRENT_DATE) - YEAR(birthdate)) -                             /* step 1 */
          (DATE_FORMAT(CURRENT_DATE, '%m%d') < DATE_FORMAT(birthdate, '%m%d')) /* step 2 */
      )>=30; 
```


Средняя температура по больнице  за сегодня (оператор = ) 
```sql
select AVG(description->"$.temperature")  from visits where CAST(date as date)=curDate();
```


Поиск по ФИО пациента (оператор LIKE)
```sql
select *  from patients where concat(name,surname,secondname) LIKE '%Иванов%';
```

Поиск однофамильцев (оператор IN)
```sql
select *  from patients where surname in('Иванов','Сидоров');
```





### Пример INSERT

Запись визита пациента в БД

```sql
INSERT INTO `visits` (`patient_id`, `description`,`date`,`created_at`) VALUES (1, '{
    "temperature": 36.6,
    "height": 180,
    "weight": 90,
    "massIndex": 27,78,
    "pressure": "120/80"
   }',now(),now()
);

```

## Команды для Docker

Поднять сервис db_va можно командой:

`docker-compose up otusdb`

Для подключения к БД используйте команду:

`docker-compose exec otusdb mysql -u root -p12345 patients_base`

Для использования в клиентских приложениях можно использовать команду:

`mysql -u root -p12345 --port=3309 --protocol=tcp patients_base`



