-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: patients_base_second
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

create database patients_base ;
use patients_base;


DROP TABLE IF EXISTS `patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `secondname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `research`
--

DROP TABLE IF EXISTS `research`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `research` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `patient_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `research_list_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `research_list`
--

DROP TABLE IF EXISTS `research_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `research_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `research_type_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `research_param`
--

DROP TABLE IF EXISTS `research_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `research_param` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '500',
  `multiple` tinyint(1) NOT NULL DEFAULT '0',
  `research_list_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `research_param_select_type_list`
--

DROP TABLE IF EXISTS `research_param_select_type_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `research_param_select_type_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `research_param_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `research_param_value`
--

DROP TABLE IF EXISTS `research_param_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `research_param_value` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `research_id` bigint(20) unsigned NOT NULL,
  `research_param_id` bigint(20) unsigned NOT NULL,
  `value_string` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value_integer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value_double` double DEFAULT NULL,
  `value_boolean` tinyint(1) DEFAULT NULL,
  `value_select` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`), KEY(`research_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
 PARTITION BY LIST (id%10)  (
        PARTITION p0 VALUES IN (0, 5),
        PARTITION p1 VALUES IN (1, 6),
        PARTITION p2 VALUES IN (2, 7),
        PARTITION p3 VALUES IN (3, 8),
        PARTITION p4 VALUES IN (4, 9)

);



/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `research_type`
--

DROP TABLE IF EXISTS `research_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `research_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `visits`
--

DROP TABLE IF EXISTS `visits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visits` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `patient_id` bigint(20) unsigned NOT NULL,
  `description` JSON NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `visits_patient_id_foreign` (`patient_id`),
  CONSTRAINT `visits_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;



-- Данные


LOCK TABLES `research` WRITE;
/*!40000 ALTER TABLE `research` DISABLE KEYS */;
INSERT INTO `research` VALUES (1,NULL,'hallo',1,NULL,NULL,1),(2,NULL,'hallo',1,NULL,NULL,2),(3,NULL,NULL,1,'2019-09-21 09:19:57','2019-09-21 09:19:57',1),(4,NULL,NULL,1,'2019-09-21 09:20:05','2019-09-21 09:20:05',1),(5,NULL,NULL,1,'2019-09-21 09:20:59','2019-09-21 09:20:59',2);
/*!40000 ALTER TABLE `research` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `research_list` WRITE;
/*!40000 ALTER TABLE `research_list` DISABLE KEYS */;
INSERT INTO `research_list` VALUES (1,'Клинический анализ крови',1,NULL,NULL),(2,'Гемоглобин',1,NULL,NULL);
/*!40000 ALTER TABLE `research_list` ENABLE KEYS */;
UNLOCK TABLES;



LOCK TABLES `research_param` WRITE;
/*!40000 ALTER TABLE `research_param` DISABLE KEYS */;
INSERT INTO `research_param` VALUES (1,'WBC, 10^9 кл/л','double',500,0,1,NULL,NULL),(2,'RBC, 10^12 кл/л','double',500,0,1,NULL,NULL),(3,'HBG, г/л','double',500,0,1,NULL,NULL),(4,'Количество, г/л','double',500,0,2,NULL,NULL),(5,'select','select',500,0,2,NULL,NULL);
/*!40000 ALTER TABLE `research_param` ENABLE KEYS */;
UNLOCK TABLES;



LOCK TABLES `research_param_select_type_list` WRITE;
/*!40000 ALTER TABLE `research_param_select_type_list` DISABLE KEYS */;
INSERT INTO `research_param_select_type_list` VALUES (1,5,'да',NULL,NULL),(2,5,'нет',NULL,NULL);
/*!40000 ALTER TABLE `research_param_select_type_list` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `research_param_value` WRITE;
/*!40000 ALTER TABLE `research_param_value` DISABLE KEYS */;
INSERT INTO `research_param_value` VALUES (1,1,1,NULL,NULL,1,NULL,NULL,NULL,'2019-09-21 09:18:48'),(2,1,2,NULL,NULL,1,NULL,NULL,NULL,'2019-09-21 09:18:48'),(3,1,3,NULL,NULL,1,NULL,NULL,NULL,'2019-09-21 09:18:48'),(4,2,4,NULL,NULL,1.4,NULL,NULL,NULL,NULL),(5,2,5,NULL,NULL,NULL,NULL,2,NULL,NULL),(6,3,1,NULL,NULL,3,NULL,NULL,'2019-09-21 09:19:58','2019-09-21 09:19:58'),(7,3,2,NULL,NULL,4,NULL,NULL,'2019-09-21 09:19:58','2019-09-21 09:19:58'),(8,3,3,NULL,NULL,5,NULL,NULL,'2019-09-21 09:19:58','2019-09-21 09:19:58'),(9,4,1,NULL,NULL,3,NULL,NULL,'2019-09-21 09:20:05','2019-09-21 09:20:05'),(10,4,2,NULL,NULL,4,NULL,NULL,'2019-09-21 09:20:05','2019-09-21 09:20:05'),(11,4,3,NULL,NULL,5,NULL,NULL,'2019-09-21 09:20:05','2019-09-21 09:20:05'),(12,5,4,NULL,NULL,554,NULL,NULL,'2019-09-21 09:20:59','2019-09-21 09:20:59'),(13,5,5,NULL,NULL,NULL,NULL,2,'2019-09-21 09:20:59','2019-09-21 09:20:59');
/*!40000 ALTER TABLE `research_param_value` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `research_type` WRITE;
/*!40000 ALTER TABLE `research_type` DISABLE KEYS */;
INSERT INTO `research_type` VALUES (1,'Анализ',NULL,NULL),(2,'Диагностика',NULL,NULL);
/*!40000 ALTER TABLE `research_type` ENABLE KEYS */;
UNLOCK TABLES;
